package com.example;

import com.example.Entities.Food;
import com.example.Entities.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan({
        "com.example.Repository",
        "com.example.Entities",
        "com.example.Controllers",})
@EnableJpaRepositories("com.example.Repository")
@Configuration
public class AppConfig {

    @Bean
    public User user() {
        return new User(1.0, "Piotr", 22, 92, 183, "Male", "medium");
    }

    @Bean
    public Food food() {
        return new Food(1.0, "zupa", 233, 3252365, 7123123, 34537, 345);
    }

    @Bean
    public Food food2() {
        return new Food(2.0, "sos", 32, 5234234, 7523, 7345345, 3453463);
    }

    @Bean
    public Food food3() {
        return new Food(3.0, "kaszanka", 312512, 5123, 1257, 53457, 334635);
    }
}

