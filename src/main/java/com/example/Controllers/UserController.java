package com.example.Controllers;

import com.example.Entities.User;
import com.example.Repository.UserRepository;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    User user;

    @GetMapping("/v1/user")
    public ResponseEntity<List<User>> getUsers() {
        JSONArray jsonArray = processToJson();

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "application/json; charset=UTF-8");
        HttpStatus httpStatus = Optional.ofNullable(user).map(status -> HttpStatus.OK).orElse(HttpStatus.NOT_FOUND);
        return (new ResponseEntity(jsonArray, responseHeaders, httpStatus));
    }

    @RequestMapping(value = "/v1/user/add", method = RequestMethod.POST)
    public ResponseEntity postUser(@RequestBody User user)
    {
        user.setBasicCaloriesRequisition(user.basicCaloriesNeeded(user.getWeight(), user.getHeight(),user.getAge(),user.getSex()));
        user.setBMI(user.BmiCalculator(user.getWeight(), user.getHeight()));
        user.setCaloriesRequisition(user.caloriesUsed(user.getBasicCaloriesRequisition(), user.getLifeStyle()));
        userRepository.save(user);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    private JSONObject processToJsonSingleUser(User user) {
        JSONObject result = new JSONObject();
        result.put("id", user.getId());
        result.put("user_age", user.getAge());
        result.put("user_basic_calories_requsition", user.getBasicCaloriesRequisition());
        result.put("user_bmi", user.getBMI());
        result.put("user_calories_requisition", user.getCaloriesRequisition());
        result.put("user_height", user.getHeight());
        result.put("user_life_style", user.getLifeStyle());
        result.put("user_name", user.getName());
        result.put("user_sex", user.getSex());
        result.put("user_weight", user.getWeight());
        return result;
    }

    private JSONArray processToJson() {
        JSONArray result = new JSONArray();

        List<User> users = userRepository.findAll();

        for (User user : users) {
            JSONObject jsonUsers = new JSONObject();
            jsonUsers.put("id", user.getId());
            jsonUsers.put("user_age", user.getAge());
            jsonUsers.put("user_basic_calories_requsition", user.getBasicCaloriesRequisition());
            jsonUsers.put("user_bmi", user.getBMI());
            jsonUsers.put("user_calories_requisition", user.getCaloriesRequisition());
            jsonUsers.put("user_height", user.getHeight());
            jsonUsers.put("user_life_style", user.getLifeStyle());
            jsonUsers.put("user_name", user.getName());
            jsonUsers.put("user_sex", user.getSex());
            jsonUsers.put("user_weight", user.getWeight());
            result.add(jsonUsers);
        }
        return result;
    }

    public void addUser() {
        User user = new User(1.0, "Piotr", 22, 92, 183, "Male", "medium");
        userRepository.save(user);
    }


}
