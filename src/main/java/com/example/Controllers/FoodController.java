package com.example.Controllers;

import com.example.Entities.Food;
import com.example.Repository.FoodRepository;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
public class FoodController {

    @Autowired
    FoodRepository foodRepository;

    @Autowired
    Food food;

    @GetMapping("/v1/food")
    public ResponseEntity<List<Food>> getAllFood() {
        JSONArray jsonArray = processToJson();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "application/json; charset=UTF-8");
        HttpStatus httpStatus = Optional.ofNullable(food).map(status -> HttpStatus.OK).orElse(HttpStatus.NOT_FOUND);
        return (new ResponseEntity(jsonArray, responseHeaders, httpStatus));
    }

    @GetMapping("/v1/food/{id}")
    public ResponseEntity singleFoodOutput(@PathVariable("id") Double id) {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "application/json; charset=UTF-8");

        HttpStatus httpStatus = Optional.ofNullable(food).map(status -> HttpStatus.OK).orElse(HttpStatus.NOT_FOUND);
        Food food = foodRepository.findById(id).orElse(null);
        JSONObject jsonObject = processToJsonSingleFood(food);
        return new ResponseEntity(jsonObject, responseHeaders, httpStatus);
    }

    @RequestMapping(value = "/v1/food/add", method = RequestMethod.POST)
    public ResponseEntity postFood(@RequestBody Food food){
        foodRepository.save(food);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    public void addFood() {
        Food food = new Food(1.0, "zupa", 233, 3252365, 7123123, 34537, 345);
        Food food2 = new Food(2.0, "sos", 32, 5234234, 7523, 7345345, 3453463);
        Food food3 = new Food(3.0, "kaszantka", 312512, 5123, 1257, 53457, 334635);

        foodRepository.save(food);
        foodRepository.save(food2);
        foodRepository.save(food3);
    }

    private JSONObject processToJsonSingleFood(Food food) {
        JSONObject result = new JSONObject();
        result.put("id", food.getId());
        result.put("name", food.getName());
        result.put("weight", food.getWeight());
        result.put("calories", food.getCalories());
        result.put("fat_content", food.getFatContent());
        result.put("carbs_content", food.getCarbsContent());
        result.put("protein_content", food.getProteinContent());
        return result;
    }

    private JSONArray processToJson() {
        JSONArray result = new JSONArray();

        List<Food> foods = foodRepository.findAll();

        for (Food food : foods) {
            JSONObject jsonFoods = new JSONObject();
            jsonFoods.put("id", food.getId());
            jsonFoods.put("name", food.getName());
            jsonFoods.put("weight", food.getWeight());
            jsonFoods.put("calories", food.getCalories());
            jsonFoods.put("fat_content", food.getFatContent());
            jsonFoods.put("carbs_content", food.getCarbsContent());
            jsonFoods.put("protein_content", food.getProteinContent());
            result.add(jsonFoods);
        }
        return result;
    }

}
