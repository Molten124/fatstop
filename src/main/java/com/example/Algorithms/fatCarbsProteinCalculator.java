package com.example.Algorithms;

public class fatCarbsProteinCalculator {
    public int totalFatCalculator(int fatContent, int weight){
        int totalFatContent = (fatContent/100)*weight;
        return  totalFatContent;
    }
    public int totalCarbsCalculator(int carbsContent, int weight){
        int totalCarbsContent = (carbsContent/100)*weight;
        return  totalCarbsContent;
    }
    public int totalProteinCalculator(int proteinContent, int weight){
        int totalProteinContent = (proteinContent/100)*weight;
        return  totalProteinContent;
    }
}
