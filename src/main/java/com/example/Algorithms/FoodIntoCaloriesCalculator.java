package com.example.Algorithms;

public class FoodIntoCaloriesCalculator {
    public double foodIntoCaloriesCalculator(int weight, int calories){
        int caloriesInFood = weight * calories;
        return  caloriesInFood;
    }
}