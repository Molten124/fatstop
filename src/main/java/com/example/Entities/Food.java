package com.example.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "app.food")
public class Food {
    @Id
    private Double id;
    @Column(name="NAME")
    private String name;
    // weight = [g]
    @Column(name="WEIGHT")
    private int weight;
    // calories = [kcal/g]
    @Column(name="CALORIES")
    private int calories;
    //fat, carbs, protein = [g/100g]\
    @Column(name="FAT_CONTENT")
    private int fatContent;
    @Column(name="CARBS_CONTENT")
    private int carbsContent;
    @Column(name="PROTEIN_CONTENT")
    private int proteinContent;

    public Food(Double id, String name, int weight, int calories, int fatContent, int carbsContent, int proteinContent) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.calories = calories;
        this.fatContent = fatContent;
        this.carbsContent = carbsContent;
        this.proteinContent = proteinContent;
    }

    public Food(){}

    public Double getId() {
        return id;
    }

    public void setId(Double id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public int getFatContent() {
        return fatContent;
    }

    public void setFatContent(int fatContent) {
        this.fatContent = fatContent;
    }

    public int getCarbsContent() {
        return carbsContent;
    }

    public void setCarbsContent(int carbsContent) {
        this.carbsContent = carbsContent;
    }

    public int getProteinContent() {
        return proteinContent;
    }

    public void setProteinContent(int proteinContent) {
        this.proteinContent = proteinContent;
    }
}
