package com.example.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "app.user")
@Entity
public class User {
    @Id
    private Double id;
    @Column(name="NAME")
    private String name;
    @Column(name="AGE")
    private int age;
    @Column(name="WEIGHT")
    private int weight;
    @Column(name="HEIGHT")
    private double height;
    @Column(name="SEX")
    private String sex;
    @Column(name="BASIC_CALORIES_REQUISITION")
    private double basicCaloriesRequisition;
    @Column(name="CALORIES_REQUISITION")
    private double caloriesRequisition;
    @Column(name="BMI")
    private double BMI;
    @Column(name="LIFE_STYLE")
    private String lifeStyle;

    public User(Double id,String name, int age, int weight, double height, String sex, String lifeStyle) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.height = height;
        this.sex = sex;
        this.lifeStyle = lifeStyle;
        this.basicCaloriesRequisition = basicCaloriesNeeded(weight, height, age, sex);
        this.BMI = BmiCalculator(weight, height);
        this.caloriesRequisition = caloriesUsed(basicCaloriesRequisition, lifeStyle);
    }

    public User(){

    }


    public double basicCaloriesNeeded(int weight, double height, int age, String sex) {
        if (sex.equals("Male")) {
            double basicCaloriesRequisition = (9.99 * weight) + (6.25 * height) - (4.92 * age) + 5;
            return basicCaloriesRequisition;
        } else {
            double basicCaloriesRequisition = (9.99 * weight) + (6.25 * height) - (4.92 * age) - 161;
            return basicCaloriesRequisition;
        }
    }

    public double caloriesUsed(double basicCaloriesRequisition, String lifestyle) {
        double totalCaloriesUsed;
        if (lifestyle.equals("none")) {
            totalCaloriesUsed = basicCaloriesRequisition * 1.2;
            return totalCaloriesUsed;
        } else if (lifestyle.equals("low")) {
            totalCaloriesUsed = basicCaloriesRequisition * 1.35;
            return totalCaloriesUsed;
        } else if (lifestyle.equals("medium")) {
            totalCaloriesUsed = basicCaloriesRequisition * 1.55;
            return totalCaloriesUsed;
        } else if (lifestyle.equals("high")) {
            totalCaloriesUsed = basicCaloriesRequisition * 1.75;
            return totalCaloriesUsed;
        } else {
            //lifestyle.equals("very high")
            totalCaloriesUsed = basicCaloriesRequisition * 2.1;
            return totalCaloriesUsed;
        }
    }

    public double BmiCalculator(int weight, double height) {
        double userBmi = weight / ((height / 100) * (height / 100));
        return userBmi;
    }

    public void howManyCaloriesToReduceWeight(double basicCaloriesRequisition, double caloriesRequisition){
        System.out.println("Żeby schudnąć musisz jeść między " + basicCaloriesRequisition + " a " + caloriesRequisition + " kcal.");
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getWeight() {
        return weight;
    }

    public double getHeight() {
        return height;
    }

    public String getSex() {
        return sex;
    }

    public double getBasicCaloriesRequisition() {
        return basicCaloriesRequisition;
    }

    public double getCaloriesRequisition() {
        return caloriesRequisition;
    }

    public double getBMI() {
        return BMI;
    }

    public String getLifeStyle() {
        return lifeStyle;
    }

    public void setBasicCaloriesRequisition(double basicCaloriesRequisition) {
        this.basicCaloriesRequisition = basicCaloriesRequisition;
    }

    public void setCaloriesRequisition(double caloriesRequisition) {
        this.caloriesRequisition = caloriesRequisition;
    }

    public void setBMI(double BMI) {
        this.BMI = BMI;
    }

    public Double getId() {
        return id;
    }
}

