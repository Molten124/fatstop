package com.example.Repository;

import com.example.Entities.Food;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FoodRepository extends CrudRepository<Food,Double> {
    List<Food> findAll();
}
